from re import template
from attr import fields
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from todos.models import TodoItem, TodoList

# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/lists_create.html"
    fields = ["name"]
    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/lists_edit.html"
    fields = ["name"]
    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todos_list")
class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/items_create"
